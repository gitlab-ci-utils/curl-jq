FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

# `curl` is required to make http requests
# `jq` is required to parse and format JSON
# `bash` is required to allow stnadardized scripting in bash
# `git` is required to interact with git repositories
# `coreutils` is required for using functions like `date -d`
# hadolint ignore=DL3018
RUN apk update &&       \
    apk add --no-cache curl jq bash git coreutils zip &&  \
    rm -rf /var/cache/apk/*

LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/curl-jq"
LABEL org.opencontainers.image.title="curl-jq"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/curl-jq"

CMD [ "bash" ]
