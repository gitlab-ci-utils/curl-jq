# Changelog

## v3.1.2 (2025-02-15)

### Fixed

- Updated base image to `alpine:3.21.3`.

## v3.1.1 (2025-01-16)

### Fixed

- Updated base image to `alpine:3.21.2`.

## v3.1.0 (2024-12-14)

### Changed

- Moved from `mplatform/manifest-tool` image to
  [`image-tools`](https://gitlab.com/-/ide/project/gitlab-ci-utils/container-images/image-tools)
  image for creating multi-platform image manifest. (#14)
- Updated base image to `alpine:3.21.0`.

## v3.0.5 (2024-09-29)

### Fixed

- Fixed CI pipeline to add correct OCI image `annotations` for this project.

## v3.0.4 (2024-09-06)

### Fixed

- Updated image to `alpine:3.20.3`.

## v3.0.3 (2024-07-26)

### Fixed

- Updated image to `alpine:3.20.2`.

### Miscellaneous

- Updated CI pipeline to include security scans.

## v3.0.2 (2024-06-20)

### Fixed

- Updated image to `alpine:3.20.1`.

## v3.0.1 (2024-06-16)

### Miscellaneous

- Updated CI pipeline to dynamically create multi-arch platforms based on the
  defined `image_build` job `matrix`.
- Updated Renovate config to track `manifest-tool` image versions, which use
  non-standard tag versioning.
- Updated tag pipelines to run `prepare_release` at pipeline start.

## v3.0.0 (2024-06-14)

### Changed

- BREAKING: Updated build to create a multi-arch image supporting `linux/amd64`
  as well as `linux/arm64` architectures. See the [README](./README.md) for
  additional details. (#13)
  - There are no functional changes, but identified as BREAKING since the
    images are now released as multi-arch.

## v2.1.0 (2024-05-22)

### Changed

- Update image to `alpine:3.20.0`.

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#12)

## v2.0.0 (2023-12-11)

### Changed

- BREAKING: Update image to `alpine:3.19`.

## v1.1.0 (2022-10-29)

### Changed

- Added standard set of `LABEL`s to image, including image license. (#9)
- Added smoke test for all packages added to the image. (#4)

### Fixed

- Updated image to `alpine:3.16.2`

### Miscellaneous

- Updated pipeline to have [Renovate](https://github.com/renovatebot/renovate) manage dependencies. (#5)

## v1.0.0 (2021-10-07)

Initial versioned release
