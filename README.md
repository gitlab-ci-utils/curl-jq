# About curl-jq

A lightweight Docker image with utilities installed for interacting with REST
APIs, git repositories, and to simplify shell scripting. This includes `curl`,
`jq`, `bash`, `git`, `coreutils`, and `zip`.

## Usage

Pull the image from the GitLab Container Registry with:

```shell
docker pull registry.gitlab.com/gitlab-ci-utils/curl-jq:latest
```

All available container image tags are available in the
gitlab-ci-utils/curl-jq repository at
<https://gitlab.com/gitlab-ci-utils/curl-jq/container_registry/>.
The following container image tags are available:

- `X.Y.Z`: Tags for specific versions, for example `1.0.0`
- `latest`: Tag corresponding to the latest commit to the `main` branch of this
repository

These images are built for `linux/amd64` and `linux/arm64` (v8) architectures.
There are also dedicated tags with the `-amd64` and `-arm64` suffixes for each
specific architecture (for example, image tag `3.0.0-arm64` for `linux/arm64`).

Note: all other image tags in this repository, and any images in the
gitlab-ci-utils/curl-jq/tmp repository, are temporary images
used during the build process and may be deleted at any point.
